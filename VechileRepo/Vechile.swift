//
//  Vechile.swift
//  VechileRepo
//
//  Created by Fiseha on 9/27/17.
//  Copyright © 2017 JFAM Mobility. All rights reserved.
//

import Foundation
import UIKit


class Vechile {
    // adding type variable to track number of newly created isntances form the classe
    
    static var count = 0

    var passengerCapacity:Int = 4
    var oneto60:Float
    var color: UIColor
    
    // designated initializzer
    
    
    init(passengerCapacity:Int, oneto60:Float, color:UIColor = UIColor.black) {
        self.passengerCapacity = passengerCapacity
        self.oneto60 = oneto60
        self.color = color
        Vechile.count += 1
        
        
    }
    
    
    //
    // convenience method
    
    convenience init () {
        
        self.init(passengerCapacity:6 , oneto60: 3.5)
    }
    
    func start () {
        
        fatalError("over ride in subclass")
    }
    
    
    
}


class MotorByce: Vechile {
    
    let fuieleffeciency:Int
    
    
    init(fuieleffeciency: Int, passengerCapacity:Int, oneto60:Float ) {
        
        self.fuieleffeciency = fuieleffeciency
        
        super.init(passengerCapacity:passengerCapacity, oneto60:oneto60)
        
        
    }
    
    
    override func start() {
         print("start motor vechile")
    }
    
}

class ElectricVechile: Vechile {
    
    
    var make:String
    var rangePerCharge:Int
    
    var description: String {
        
        return "\(ElectricVechile.self):\n\tPassengers: \(passengerCapacity)\n\t0 to 60: \(oneto60) secondes\n\tRange: \(rangePerCharge) miles"
    }

    
   
    init(make:String, rangePerCharge:Int, passengerCapacity:Int, oneto60:Float ) {
        
        self.make = make
        self.rangePerCharge = rangePerCharge
        
        super.init(passengerCapacity:passengerCapacity, oneto60:oneto60)
    }
    
    override func start() {
        print("scielence")
    }
    
    





}



